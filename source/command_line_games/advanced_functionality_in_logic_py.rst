.. _sect_advanced_functionality_in_logic_py:

====================================================
Advanced functionality supported in :file:`logic.py`
====================================================

This section describes advanced functionality available through the :file:`logic.py` file for each game that you may need as your game becomes more sophisticated.

Before reading this section, you should be familiar with the basic functionality supported in the :file:`logic.py` file for each game. If you have followed this documentation in order, you will have read about this basic functionality. As a reminder, the relevant sections of the documentation were:

    - section ":ref:`sect_modify_sublevelbehaviour`",
    - section ":ref:`sect_modify_sublevelbehaviour_docstrings`",
    - section ":ref:`sect_modify_gamestatecustom`",
    - section ":ref:`sect_template_modify_create_game_round`",
    - section ":ref:`sect_modify_the_game_logic`" relating to modifying the function :py:func:`_decide_on_problem_instance`,
    - section ":ref:`sect_update_decide_on_answers`", and
    - section ":ref:`sect_modify_gamebehaviour`" about the file :file:`behaviour.py`.


.. _sect_creating_own_enum:

---------------------------------
Creating your own enumerated type
---------------------------------

Let's say you wanted to vary the difficulty of your colour-based game to use only the two colours black and white on easy levels, progressing to the three primary colours on slightly harder levels, and then including all colours in the rainbow on the hardest levels. You could use an integer parameter to ``SublevelBehaviour`` where 0 meant black and white, 1 meant primary colours, and so on.

..
    hereiam MOST RECENT
    TODO:
    However, you might prefer to use strings to make it easier for a user to understand what the values of the parameter mean. The disadvantage of this is that some strings will be invalid and Python will not automatically do that for you; you'll have to write the code for that yourself.

    However, you might prefer to use an enumerated type as a parameter in ``SublevelBehaviour`` to make it easier for a user to understand what the values of the parameter mean.

    progressing by having even numbers only, odd numbers only, or even and odd numbers. ... or colours?

    make your game harder have three lists "easy", "medium", and "hard" in your game


<< TODO: enumerated_type example, e.g. ``GameMode`` from kangaroo and password. Others: ordered, slice, slicing, tunnel, funnywindows >>

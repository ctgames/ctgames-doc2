.. _sect_checklist_for_game_completion:

==========================================
Checklist for command-line game completion
==========================================

This section reminds you of all of the tasks that must be completed in order to finish developing a command-line CTGames game.

.. note:: There is a companion section ":ref:`sect_checklist_for_web_app_completion`" with the checklist for web app games. When finalising a CTGames web app (built on top of a command-line game), both checklists will need to be honoured.


-----------------
Functional issues
-----------------

Command-line versions of games

    * On the command line, running :command:`python cl.py --help` gives a reasonable help message for the custom parameters.
    * On the command line, running :command:`python cl.py -c .....` allows one to specify custom inputs.
    * On the command line, running :command:`python cl.py -c .....` and passing any kind of bad inputs does not crash the game.
    * If the rules change from one round to another, these rules are written to the dict :py:data:`LEVEL_INSTRUCTIONS` in :file:`logic.py` (see the game Kangaroo for an example).
    * The :file:`logic.py` file has a suite of tests (e.g. unit tests).
    * The :file:`behaviour.py` file lists 9+ (only for the simplest games) and 12+ (for all other games) different sublevels in increasing order of difficulty.


--------------------
Documentation issues
--------------------

Command-line versions of games

    * The file "text_constants.py" has appropriate text descriptions for children and for teachers.


-----------
Code issues
-----------

Command-line versions of games

    * In "logic.py", the name ``__date__`` should specify the approximate date the file was last edited.
    * In "logic.py", if you wish to transfer copyright for the code you have written (read ":ref:`sect_copyright_statements`" for details) the name ``__copyright__`` should state that the code is released under a Creative Commons Zero licence with the text ``'Released under a CC0 licence'`` or should state that copyright is assigned to Thomas J. Naughton so it can be released as free and open source software in the future, with the text ``'Copyright 2022, Thomas J. Naughton'``.
    * In "logic.py", the name ``__credits__`` should have your full name preceding Thomas J. Naughton's name, e.g. ``['Josephine A. Bloggs', 'Thomas J. Naughton']``.
    * You have worked to eliminate as many "warnings" or "code style analysis" errors (yellow triangles in PyCharm) as possible.
    * There are no unexplained "magic numbers" in the code -- all values that might need to be tweaked are constants with names in ALL_CAPITALS near the top of the file or at the top of the function in which they are used.
    * All constants have a """docstring""" immediately below them describing what the constant is for.

.. _chap_ctgames_framework:

.. centered:: Chapter: CTGames framework

=======================================
Under the hood of the CTGames framework
=======================================

It is possible to successfully develop games within the CTGames framework without knowing many details about the framework, however for completeness, section ":ref:`framework_separation`" seeks to give a high-level overview of the components in the CTGames framework.

.. _sect_before_starting_game_development:

================================
Before starting game development
================================

------------------------
Updating the Python path
------------------------

.. note:: If you are using the pre-configured VirtualBox image of CTGames supplied by your supervisor, the first command mentioned in this
    section, :command:`python setup.py`, will already have been run for you when the image was configured.
    However, it will not be a problem to run :command:`python setup.py` again.

The first time you clone the git repository (if you need to clone the repository) you need to update the Python path. Open a terminal in any directory and enter::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/
    python setup.py

Sign out of Ubuntu and sign in again for the changes to permanently take effect. (Simply exiting the terminal and starting a new terminal may work, but logging out and in is guaranteed to work.)

.. tip:: Any time you move the git repository to a different place in your filesystem, or if you create a new user, you need to update the Python path again by running :command:`python setup.py` so that Python can find the CTGames framework.

If the Python path has been updated successfully you will be able to run command-line versions of other developers' games, as described next.

Running command-line versions of other developers' games
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: running_command_line_games.rst


.. _sect_before_starting_game_development_pycharm:

-------
PyCharm
-------

.. sidebar:: Alternative IDEs
    :subtitle: Spyder

    If you choose to use a different :abbr:`IDE (integrated development environment)`, turn on the equivalent features to give you hints
    to ensure your code style is of a professional quality.
    For example, with the Spyder IDE, click :guilabel:`Tools` -> :guilabel:`Preferences` -> :guilabel:`Editor` ->
    :guilabel:`Code Introspection/Analysis` and make sure the two check boxes :guilabel:`Real-time code analysis` and
    :guilabel:`Real-time code style analysis` are checked.

PyCharm is the recommended IDE for this project.

PyCharm gives you hints to help you ensure your Python code style is to a professional standard. Please use them. Also, an automatic code formatter called Blackpp, installed during the CTGames installation process, will reformat your code each time you save.


..
    **HIDDEN**
    Text I had previously for Spyder:
    Spyder can give you hints to help you ensure your Python code style is to a professional standard.
    In the Spyder IDE, click Tools->Preferences->Editor->Code Introspection/Analysis and make sure the two check boxes "Real-time code
    analysis" and "Real-time code style analysis" are checked.

Open a terminal in any directory, and enter
::

    conda activate ctgames
    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/
    git pull
    git status

This ensures that your working directory is the CTGames games directory, and that you are in the **ctgames** conda environment. Then, to run any particular game, e.g. Kangaroo, enter
::

    cd kangaroo
    python cl.py


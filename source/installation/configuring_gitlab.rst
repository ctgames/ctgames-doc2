The following steps are required to configure GitLab whether you use the CTGames development environment through VirtualBox or by installing directly on your computer.


Activate your account on GitLab
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As a first step, you must sign in at least once to `<https://gitlab.cs.nuim.ie>`_ with your Maynooth University Department of Computer Science sign-in details.


Become a developer for the CTGames project
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Ensure that your supervisor has added you as a "Developer" on the CTGames project by checking is CTGames listed as one of your projects at `<https://gitlab.cs.nuim.ie/dashboard/projects>`_.


Configure Git
^^^^^^^^^^^^^

These instructions have been adapted from `<https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html>`_.

#. Ensure that Git is installed

    Open a terminal in any directory and ensure that Git is installed by
    entering
    ::

        git --version

    If you have Git installed, the particular version number of the installation is printed, which will be this version number or higher
    ::

        git version 2.25.1

    If you have completed all of the previous steps correctly, Git will have been installed, so if Git is not installed this may be an indication or a more serious problem that you have skipped some steps. Nonetheless, a quick fix here is to run
    ``sudo apt install git`` and repeat this step.

#. Git global settings

    To identify you as the author of your work, you should enter your user name and email. It is important to use the same email on all of the different computers that you use for development so that all of your contributions are grouped together as coming from one person. Use your full name as your user name
    ::

        git config --global user.name "Josephine A. Bloggs"

    and your Maynooth University email address
    ::

        git config --global user.email "josephine.bloggs.1970@mumail.ie"

    Other useful configuration options are
    ::

        git config --global color.ui auto
        git config --global merge.tool meld

    .. Aside: since Git version 2.8 this line is not necessary any more:
        git config --global push.default simple

#. Check your information

    To check these details, enter
    ::

      git config --global --list

.. tip:: If you would like to know more on how Git manages configurations you could read the `Git Config <https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration>`_ documentation.


Create and install a SSH key
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You must create and install a SSH key on the Ubuntu OS you wish to use for software development. This allows you to commit and push updates to the git repository without having to type in your password each time, as long as you commit from this Ubuntu OS. These instructions have been adapted from `<https://docs.gitlab.com/ee/ssh/>`_.

.. note:: These instructions assume that you do not wish to re-use a SSH key that you've previously created, but of course you can do that if you wish.


Generating a new SSH key pair
.............................

You can create and configure an ED25519 public/private key pair by opening a terminal in any directory and entering (remember to use your own email address as the label for the SSH keys)
::

    ssh-keygen -t ed25519 -C "josephine.bloggs.1970@mumail.ie"

We will change the default name for the key pair. When you get a response (where ``dev`` denotes your username) of the form
::

    Generating public/private ed25519 key pair.
    Enter file in which to save the key (/home/dev/.ssh/id_ed25519):

enter the location and name ``/home/dev/.ssh/gitlab_cs_key`` (where ``dev`` denotes your username) as shown
::

    Generating public/private ed25519 key pair.
    Enter file in which to save the key (/home/dev/.ssh/id_ed25519): /home/dev/.ssh/gitlab_cs_key

and press **Enter**. Confirm to overwrite the key that already exists (if any) with the filename "gitlab_cs_key". You will be prompted to set up a passphrase for your SSH key. If you have password protected your sign-in for Ubuntu there are only marginal benefits to be gained by passphrase protecting your key, so you can just press **Enter** twice to denote no passphrase
::

    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:

If the SSH key pair was generated successfully, you’ll see confirmation of where it was saved (where ``dev`` denotes your username)
::

    Your identification has been saved in /home/dev/.ssh/gitlab_cs_key.
    Your public key has been saved in /home/dev/.ssh/gitlab_cs_key.pub.

.. tip:: If needed, you can change the passphrase for your SSH key (where
    ``dev`` denotes your username) with
    ::

        ssh-keygen -p -f /home/dev/.ssh/id_ed25519


Adding a SSH key to your GitLab account
.......................................

You must copy and paste the public part of the SSH key pair you created to
your GitLab account. To get the public part, in a terminal enter
::

    cat ~/.ssh/gitlab_cs_key.pub

You should see something like this (with your own ``mumail`` email address as the label)
::

    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAA... josephine.bloggs.1970@mumail.ie

Copy this text to the clipboard (highlight the text and press **Shift**-**Ctrl**-**c**). Make sure you copy the entire text starting with ``ssh-ed25519`` and ending with (and including) your email address.

Point Firefox to `<https://gitlab.cs.nuim.ie>`_, sign in, and perform the following steps.

    - Select your avatar in the upper right corner, and click "Settings".
    - Click "SSH Keys".
    - Into the text box labelled "Key", paste the public key you just copied.
    - Into the text box labelled "Title", add a name that reminds you of the computer on which you installed the key and the associated email address, such as "My_laptop_josephine.bloggs.1970@mumail.ie".
    - Click the button labelled "Add key".


Testing that everything is set up correctly
...........................................

To test whether your SSH key was added correctly, in a terminal enter
::

    ssh -T git@gitlab.cs.nuim.ie

If asked to verify the authenticity of the GitLab host (in particular, if you are installing CTGames directly on your computer and this is the first time you have connected to GitLab via SSH), answer ``yes`` to add "gitlab.cs.nuim.ie" to the list of trusted hosts, as shown
::

    The authenticity of host 'gitlab.cs.nuim.ie (149.157.XXX.XXX)' can't be established.
    ECDSA key fingerprint is SHA256:aBcDeFgHiJkLmNoPqRsTuVwXyz0123456789AbCdEfG.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added 'gitlab.cs.nuim.ie' (ECDSA) to the list of known hosts.

Once added to the list of known hosts, you should validate the authenticity of the GitLab host once again. Run the **ssh** command shown above again, and you should receive a "Welcome to GitLab" message.


Advanced SSH-related settings
.............................

If this is your first time following these instructions then please skip this section. These advanced settings are only useful in cases where you wish to develop multiple projects using different email addresses, and/or using different private keys (e.g. where the projects' Git repositories are hosted with different service providers).

After fixing your global Git settings as shown above, which will be the default email address and SSH private key for all commits, you can have different settings on a **per-directory** basis. Let us say that you also want to commit to a project on **gitlab.com** with a different email address, and want a different SSH private key for that service. This can be achieved by appending these lines to the end of your **~/.gitconfig** file
::

    [includeIf "gitdir:~/gitlab.com/**"]
        path = ~/gitlab.com/.config_git_per_directory

Then, create a new SSH private key (save it in a file called **/home/dev/.ssh/gitlab_com_key**) and create a new file **~/gitlab.com/.config_git_per_directory** with the lines
::

    # Special settings for gitlab.com
    [user]
        name = "Jo Bloggs"
        email = "jbloggs@example.com"
    [core]
        sshCommand = "ssh -o IdentitiesOnly=yes -i ~/.ssh/gitlab_com_key -F /dev/null"

.. Aside: will this shorter version work just as well?
    sshCommand = "ssh -i ~/.ssh/work_key"

This will work for all repositories within the directory specified. Alternatively, for more fine-grained settings that will have to be repeated on a **per-project** basis, open a terminal from inside the git repository and enter
::

    git config --local user.name "Jo Bloggs"
    git config --local user.email jbloggs@example.com
    git config core.sshCommand "ssh -o IdentitiesOnly=yes -i ~/.ssh/private-key-for-this-repo -F /dev/null"

Note, the shorter SSH command ``sshCommand = "ssh -i ~/.ssh/work_key"`` is also suggested in the literature. By way of explanation of the differences, ``man ssh`` and ``man ssh_config`` tells us
::

    -o option
        Can be used to give options in the format used in the configuration
        file.  This is useful for specifying options for which there is no
        separate command-line flag.  For full details of the options listed
        below, and their possible values, see ssh_config(5).

        IdentitiesOnly
            Specifies that ssh(1) should only use the authentication identity
            and certificate files explicitly configured in the ssh_config files
            or passed on the ssh(1) command-line, even if ssh-agent(1) or a
            PKCS11Provider offers more identities.  The argument to this keyword
            must be yes or no (the default).  This option is intended for
            situations where ssh-agent offers many different identities.

    -F configfile
        Specifies an alternative per-user configuration file.  If a
        configuration file is given on the command line, the system-wide
        configuration file (/etc/ssh/ssh_config) will be ignored. The default
        for the per-user configuration file is ~/.ssh/config.

    -i identity_file
        Selects a file from which the identity (private key) for public key
        authentication is read.  The default is ~/.ssh/id_dsa, ~/.ssh/id_ecdsa,
        ~/.ssh/id_ed25519 and ~/.ssh/id_rsa.  Identity files may also be
        specified on a per-host basis in the configuration file.  It is possible
        to have multiple -i options (and multiple identities specified in
        configuration files).  If no certificates have been explicitly specified
        by the CertificateFile directive, ssh will also try to load certificate
        information from the filename obtained by appending -cert.pub to
        identity filenames.

.. _sect_install_virtualbox:

=================================================
Installation option 2: VirtualBox virtual machine
=================================================

This section assumes that you're planning to develop CTGames on Ubuntu that runs as a "guest OS" on your computer using VirtualBox. These steps should work whether the operating system on your computer (called the "host OS" by VirtualBox) is Windows, macOS, or GNU Linux. VirtualBox calls the Ubuntu running inside the host OS as the "guest OS".

Throughout this section you'll be typing commands into a terminal. To open a terminal (also called a shell) hold down the three keys **Ctrl**-**Alt**-**t**. Make the terminal window as large as you need.

.. toctree::
    :maxdepth: 2
    :name: install-virtualbox-toc

    install_virtualbox_virtualbox
    install_virtualbox_configure
    install_virtualbox_gitlab
    install_virtualbox_getting_started

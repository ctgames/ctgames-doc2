.. _sect_install_virtualbox_configure:

------------------------------------------------------
Configuring the virtual machine for your host computer
------------------------------------------------------

Run VirtualBox. If prompted to upgrade VirtualBox, do not upgrade. If we all use the same version of VirtualBox it is one less variable to deal with if something does not work.

When VirtualBox has started, click ``File`` -> ``Preferences...`` ->
``Update`` and uncheck the box ``Check for updates``. Click ``OK`` to close the ``VirtualBox - Preferences`` pop-up window.

Click ``Machine`` -> ``Add...`` and choose the .vbox file that you saved earlier.

Choose the CTGames virtual machine from the list of available machines and then click on the yellow cog labelled ``Settings``. A ``CTGamesYYYYMM - Settings`` window should pop up.

In the ``System```-> ``Motherboard`` section give this virtual machine the maximum recommended amount of memory as indicated by the green bar.

In the ``System```-> ``Processor`` section give this virtual machine the maximum recommended number of CPUs as indicated by the green bar.

In the ``Shared folders`` section, select the ``/home/tomn/Desktop`` shared directory that is there already and click the little ``x`` button to remove it.

Still in the ``Shared folders`` section, click the little ``+`` button to add a new shared directory. As the ``Folder Path`` choose the ``virtualbox_shared`` directory that you created previously. Check
``Auto-mount`` and click ``OK``.

Click ``OK`` to close the ``CTGamesYYYYMM - Settings`` pop-up window.

Choose the CTGames virtual machine from the list of available machines and then click on the green arrow labelled "Start".

When you start the CTGames virtual machine for the first time you may see two messages regarding "Auto capture keyboard" and "mouse pointer integration". Just click the ``\`` button on each that has the tooltip "Do not show this message again."

When Ubuntu starts, sign in with the password given to you by your
supervisor. Open a terminal (also called a shell) by holding down the three keys **Ctrl**-**Alt**-**t**, or clicking the shortcut on the Launcher with the tooltip "Terminal". Resize this terminal window as large as you need.

.. tip:: If you want to hibernate the virtual machine at any point with the
    intention to continue from where you left off (for example, if you're in
    the middle of a CTGames web app game) close the main VirtualBox app (the
    window with the title "CTGamesYYYYMM (Running) - Oracle VM VirtualBox")
    and choose the option with the label "Save the machine state".

.. tip:: Your Ubuntu guest OS is configured to automatically check for
    updates each week. If you get a pop-up window asking you to install
    updates, you should accept. Sometimes you will be asked for the sign-in
    password. Sometimes you may be asked to shutdown and restart the OS. If you
    encounter an error with CTGames, by keeping your Ubuntu guest OS
    up-to-date, it will be easier for another person to replicate the error
    exactly in order to more quickly find a fix.

.. tip:: If anyone else could have access to your copy of the virtual
    appliance, you should change the Ubuntu user sign-in password, because you
    will be storing a private SSH key in the appliance. One reason you might
    reasonably choose not change the password is if your virtual appliance is
    on a computer for which only you have the password and you use disk-level
    encryption.

.. Previously I had argued that protecting the data was not necessary, with:
    Since it is assumed that
    - you will not store or record any non-CTGames personal information in the virtual appliance (internet shopping history, for example),
    - you will push everything you develop to the remote git repository at least at the end of each development period, and
    - the code for your game will be open sourced anyway,
    in practice the strongest reason to change your password might be that it is just good practice to do so.

.. note:: The virtual appliance is shipped with one user, with (as of 2021) a
    username of ``dev``. There is no need to create a new user for yourself or
    change the username. In fact, some of the steps that follow are more
    convenient if you do not change the username.

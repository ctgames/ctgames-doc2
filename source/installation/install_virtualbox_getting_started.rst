.. _sect_install_virtualbox_getting_started:

----------------------------
Getting started with CTGames
----------------------------

PyCharm is the recommended :abbr:`IDE (integrated development environment)` to use for CTGames development. There is a link to the PyCharm IDE in the Ubuntu launcher. Games you create will appear in :file:`CTGames/ctgames/` in the project directory structure visible in PyCharm.

Firefox is the recommended browser to run CTGames web app games, and is installed in the virtual appliance. If you encounter any strange behaviour with Firefox, alternative web browsers Chromium and Chrome are also installed to allow you check if the strange behaviour is due to CTGames or any one particular browser.

You can test that the virtual appliance has been configured correctly by starting the CTGames server and playing the web app games. To play the web app games you will need a teacher's sign-in details and class members' details that have been pre-populated in the virtual appliance. Your supervisor will give you these.

Running locally the web app versions of other developers' games
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: ../web_apps/running_web_app_games.rst

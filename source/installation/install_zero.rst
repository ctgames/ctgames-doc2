.. _sect_install_zero:

===================================
Installation option 1: Zero-install
===================================

This option is available during periods when we offer publicly-accessible web app games, such as University Open Days, Science Week, Tech Week, and so on.

Request the public URL, a teacher's sign-in details, and a class member's account details from your supervisor. Then follow these steps:

  - Assuming the public URL you were given was ``https://149.157.999.999:9999/``, point Firefox to the teacher's portal which would be ``https://149.157.999.999:9999/teachers`` and sign-in using the teacher's sign-in details.
  - Choose the class that matches the class member's ID that you have and and click **Activate**.
  - Point Firefox to the public URL you were given and sign-in using the class member's ID and PIN.


--------------------------------------------------
Committing edits to the code using the web browser
--------------------------------------------------

The disadvantages of editing the code base using the web browser were already stated in section ":ref:`sect_install_options_zero`".

Nonetheless, if you point Firefox to `<https://gitlab.cs.nuim.ie/ctgames/ctgames>`_ you can view and edit the code through a web browser.

.. tip:: Make sure to edit Python files using the :guilabel:`Web IDE` button rather than the :guilabel:`Edit` button as this will allow you to stage files and combine multiple edits into a single commit with a meaningful commit message. Otherwise, you run the risk of making the git log look very noisy with multiple tiny commits.

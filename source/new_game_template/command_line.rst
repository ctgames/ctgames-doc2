===============================================
Command-line functionality in the template game
===============================================


--------------------------
Modify ranges and defaults
--------------------------

(found in cl.py)

Repeating the question asked above: where do the names in ``SublevelBehaviour`` get their values? The second place is from a teacher specifying a custom round through the web app version of the game, or equivalently, the developer passing the ``--custom`` argument in the command-line version of the game. It will be important that the input from the teacher is of the correct type and that the value is within the ranges acceptable by your game logic. By putting effort into this step, you will not need to write any code to sanitise inputs and can be sure that your ``logic.py`` will not raise an exception due to an unexpected input type or value. This is because

    - the web app will ensure the correct type by automatically generating a drop-down box, checkbox, spin button, etc. that only allows the teacher to input values of the correct type, and
    - if the value is the correct type, but an incorrect value, the framework will automatically choose the closest valid value.

There are two objects to modify in this step, ``ROUND_BEHAVIOUR_RANGES`` and ``DEFAULT_ROUND_BEHAVIOUR``.


.. _sect_round_behaviour_ranges:

Modify ``ROUND_BEHAVIOUR_RANGES``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``ROUND_BEHAVIOUR_RANGES`` object specifies the type and allowed values for each parameter of ``SublevelBehaviour``. In the template, it is specified as:

.. code-block:: python

    ROUND_BEHAVIOUR_RANGES: MAIN_GAME.SublevelBehaviour = (
        MAIN_GAME.SublevelBehaviour(
            param1=range(2, 20),
            param2=range(1, 7),
            )
        )

The names of the parameters must match the names of your ``SublevelBehaviour``. You must assign an object to each parameter. Your choices of object are

    - a standard Python type, e.g. ``param1=int`` or ``param1=bool``
    - an range object, e.g. ``param1=range(2, 11)``
    - an enumerated type from CTGames, e.g. ``param1=ReplayIncorrectRound`` (the game "Ordered List" uses this)
    - an enumerated type that you define yourself, e.g. ``param1=GameMode`` (see section ":ref:`sect_creating_own_enum`")
    - a namedtuple or class with named attributes, e.g. ``param1=MyClass`` or  ``param1=MyNamedTuple`` (note, if ``MyClass`` is defined in "logic.py" you'll have to refer to it as ``param1=MAIN_GAME.logic.MyClass``)
    - a sequence object (e.g. a list ``param1=[2, 3, 5, 7, 11, 13, 17]`` or ``param1=MY_LIST``) to restrict the possibilities to a particular set (note, if ``MY_LIST`` is defined in "logic.py" you'll have to refer to it as ``param1=MAIN_GAME.logic.MY_LIST``)
    - a relative range object, e.g. ``param2=RelativeRange(ge=3, lt='param1')`` (see section ":ref:`sect_using_relative_range`")


.. _sect_default_round_behaviour:

Modify ``DEFAULT_ROUND_BEHAVIOUR``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ``DEFAULT_ROUND_BEHAVIOUR`` object specifies the default value that should be given to this parameter if one is not available. In the template, it is specified as:

.. code-block:: python

    DEFAULT_ROUND_BEHAVIOUR: MAIN_GAME.SublevelBehaviour = (
        MAIN_GAME.SublevelBehaviour(
            param1=5,
            param2=1,
            )
        )

The particular choice of values is up to you, but probably values corresponding to one of the easier levels from your ``SublevelBehaviour`` would be best. You also have the choice of using a relative default object, e.g. ``param2=RelativeDefault('param1')`` here (see section ":ref:`sect_using_relative_default`").

.. note:: CTGames currently implements the following input-sanitising protocol. If the teacher submits a value that is the correct type, but is outside the correct range, the value in the range closest to the submitted value is used. If the teacher submits a value for a parameter that is the incorrect type, then the respective ``DEFAULT_ROUND_BEHAVIOUR`` parameter is used.

..
    Examples from CTGames
    ^^^^^^^^^^^^^^^^^^^^^
    << TODO >>


.. _sect_update_cl_help_custom:

-------------------------
Update ``CL_HELP_CUSTOM``
-------------------------

(found in cl.py)

A CTGames developer can create a custom round by entering their own chosen values for the ``SublevelBehaviour`` parameters when running the command-line version of the game. This is done by entering a command of the form ``python cl.py --custom`` or ``python cl.py -c`` followed by the sequence of values separated by spaces. For example, for a game newly-created from the template, in a directory called "myfirstgame", one would create a custom round by opening a terminal in any directory and entering::

    conda activate ctgames
    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/myfirstgame/
    python cl.py --custom 5 2

Here, ``SublevelBehaviour`` is expecting two ints, so that is why we entered two integers after ``--custom``. If the type was ``bool`` we could have entered ``True`` or ``False``. If the type was an enumerated type, we could have entered any of the members in the type, e.g. for Most Frequent we could have entered ``OnlyTwo`` or ``ThreeChooseOne``.

In order to explain to other CTGames developers what are the ``SublevelBehaviour`` parameters in a custom round, the command-line version of each game has a help message. To see it, assuming your newly-created game is in a directory called "myfirstgame", open a terminal in any directory and enter::

    conda activate ctgames
    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/myfirstgame/
    python cl.py --help

For the template, this part of the help message is::

    optional arguments:
    -h, --help            show this help message and exit
    -v, --version         show program's version number and exit
    -c PARAM1 PARAM2, --custom PARAM1 PARAM2
                          specify `PARAM1` and `PARAM2` for a custom fixed-behaviour
                          game. `PARAM1` is a positive int that specifies the length
                          of the list. `PARAM2` is a positive int that specifies how
                          many digits are in each number. As examples, consider
                         --custom 5 1, and --custom 10 2.

In the template, the code generating the relevant part of this help message is:

.. code-block:: python

    CL_HELP_CUSTOM: str = '''\
        specify `{0}` and `{1}` for a custom fixed-behaviour game.
        `{0}` is a positive int that specifies the length of the list.
        `{1}` is a positive int that specifies how many digits are in each
        number.
        As examples, consider --custom 5 1, and --custom 10 2.
        '''.format(
        *CL_CUSTOM_LABELS
        )

You should modify ``CL_HELP_CUSTOM`` with respect to

  #. the number of parameters in your ``SublevelBehaviour``,
  #. an explanation for each parameter, and
  #. example command-line parameters.

The examples below should be sufficient to explain how the modifications should be done.


``CL_HELP_CUSTOM`` examples from CTGames
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Just Reverse has only one parameter, an integer:

.. code-block:: python

    CL_HELP_CUSTOM: str = '''\
        specify `{0}` for a custom fixed-behaviour game.
        `{0}` is a positive int that specifies the length of the word to
        reverse.
        As examples, consider --custom 5, and --custom 10.
        '''.format(
        *CL_CUSTOM_LABELS
        )

Aliens Zoo has three integer parameters:

.. code-block:: python

    CL_HELP_CUSTOM: str = '''\
        specify `{0}`, `{1}`, and `{2}` for a custom fixed-behaviour game.
        `{0}` is a positive int specifying the numbers of different aliens.
        `{1}` is a positive int specifying the number of fields to fill.
        `{2}` is a positive int specifying the number of aliens to put in
        each field.
        As examples, consider --custom 3 2 2, and --custom 4 5 2.
        '''.format(
        *CL_CUSTOM_LABELS
        )

Most Frequent has an integer parameter and an enumerated type parameter:

.. code-block:: python


    CL_HELP_CUSTOM: str = '''\
        specify `{1}` and `{2}` for a custom fixed-behaviour game.
        `{1}` is a game mode from the set {{{0}}}.
        `{2}` is the length of the sequence.
        As examples, consider --custom OnlyTwo 5, and --custom
        ThreeChooseOne 10.
        '''.format(
        ', '.join(class_attributes(GameMode)), *CL_CUSTOM_LABELS
        )

Funny Windows has three parameters including an enumerated type:

.. code-block:: python

    CL_HELP_CUSTOM: str = '''\
        specify `{1}`, `{2}`, and {3} for a custom fixed-behaviour game.
        `{1}` is a game mode from the set {{{0}}}.
        `{2}` is the number of windows in each side of the boat.
        `{3}` is the number of boat sides.
        As examples, consider -c Logic 5 2, and -c AdvancedColour 10 3.
        '''.format(
        ', '.join(class_attributes(GameMode)), *CL_CUSTOM_LABELS
        )

Just Addition has a Boolean as one of its three parameters, and also makes explicit what is the range of integer values accepted:

.. code-block:: python

    CL_HELP_CUSTOM: str = '''\
        specify `{0}`, `{1}`, and `{2}` for a custom fixed-behaviour game.
        `{0}` is a positive int that specifies the maximum value for each
        number.
        `{1}` denotes if subtraction is to be used rather than the default 
        addition (use True or False).
        `{2}` is a value in {3} that specifies the number of
        multiple-choice answers.
        As examples, consider -c 5 False 4, and -c 10 True 5.\
        '''.format(
        *CL_CUSTOM_LABELS, seq_to_str(ROUND_BEHAVIOUR_RANGES.num_mcq_answers)
        )

Ordered has an enumerated type as one of its three parameters, and also makes explicit what is the range of values accepted for its integer parameter:

.. code-block:: python

    CL_HELP_CUSTOM: str = '''\
        specify `{1}`, '{2}', and `{3}` for a custom fixed-behaviour game.
        `{1}` is a positive int that specifies the size of the
        collection of objects.
        `{2}` is the ordering criterion from the set {{{0}}}.
        `{3}` is a value in {4} that specifies the number of
        multiple-choice answers.
        As examples, consider --custom 3 Sides 4, and
        --custom 6 Colour 4.
        '''.format(
        ', '.join(class_attributes(GameMode)),
        *CL_CUSTOM_LABELS,
        seq_to_str(ROUND_BEHAVIOUR_RANGES.num_mcq_answers),
        )


---------------------------------
Run ``test_follows_framework.py``
---------------------------------

A test suite has been created to check for some of the most common problems that stop a game working with the CTGames framework. To run the test suite, assuming your game is ``catchaball``, in any terminal enter::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/ctgames/catchaball/test/
    python test_follows_framework.py

As of 2020, it currently only tests your game's ``SublevelBehaviour``. If it finds any errors, you will need to fix them.

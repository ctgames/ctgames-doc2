=====================================
String constants in the template game
=====================================


.. _sect_modify_the_strings_in_the_game:

------------------------------
Modify the strings in the game
------------------------------

(found in "text_constants.py")

The string ``GAME_INSTRUCTIONS_TEACHERS`` explains the game for teachers. It could be used for the command line or web app versions of the game.

..
    << TODO: Examples >>

The string ``GAME_QUESTION_CHILDREN`` is a very simple game question that can be understood by a child.

..
    << TODO: Examples >>

The string ``GAME_INSTRUCTIONS_CHILDREN`` is for very simple game instructions that can be understood by a child. It might describe a story for the game, in the way that most Bebras-style tasks are introduced with a one or two sentence story.

..
    << TODO: Examples >>

The string ``SCIENCE_FACT_CHILDREN`` is a gimmick that is designed to draw the child into the game by relating some fact about a topic that children would already be familiar with. It should be an interesting child-friendly scientific/historical/geographical fact that could appeal to the widest possible range of children, and that is somehow related to the story in the game. It should not be related to computer science, unless it is some aspects of computer technology that is widely known or used.

..
    << TODO: Examples >>

.. tip:: The ``SCIENCE_FACT_CHILDREN`` string can be filled in later. Usually it is only evident later in the game development (during the web app creation) what would be most interesting to put here.


Parameterised text strings
^^^^^^^^^^^^^^^^^^^^^^^^^^

Sometimes the text strings need to be dynamically modified, depending on the data randomly generated for that particular game round. The parameterised text strings can be specified here and then values given to those parameters in the function ``construct_cl_formatted_strings`` when the game round is defined.

Count from Zero defines the following parameterised text string:

.. code-block:: python

    GAME_QUESTION_CHILDREN = 'Which truck number has {} ?'
    """str : Very simple game question that can be understood by a child."""

Molecules defines the following parameterised text string:

.. code-block:: python

    GAME_QUESTION_CHILDREN = 'How many {} molecules can be made using these?'
    """str : Very simple game question that can be understood by a child."""

Pollen defines the following parameterised text string:

.. code-block:: python

    GAME_QUESTION_CHILDREN = '''\
            How much pollen can the bee get? It can carry {} pollen per
            flight and can make {} flights.'''
    """str : Very simple game question that can be understood by a child."""

Kangaroo defines the following parameterised text string:

.. code-block:: python

    GAME_QUESTION_CHILDREN = 'What jumps will collect {} {}{}?'
    """str : Very simple game question that can be understood by a child."""



.. _sect_modify_construct_cl_formatted_strings:

------------------------------------------------------
Modify the function ``construct_cl_formatted_strings``
------------------------------------------------------

(found in "cl.py")

The CTGames framework prints some default strings and applies some default text formatting for the command-line versions of games. These strings include the problem instance, question, rules text, custom parameters, and so on. These defaults are to get your command-line games up and running as quickly as possible. However, you may want to customise these strings. One type of customisation is pretty printing the string or making it more human readable. Another modification is injecting information from the game state that might change from game round to game round.

The function ``construct_cl_formatted_strings`` is the only function that appears by default in the file "cl.py" from a game newly created using the template. The purpose of this function is to customise the strings that will be printed during the command-line version of your game. These customised strings will also be re-used by default in the web app version of your game.

.. tip:: The function ``construct_cl_formatted_strings`` is only needed if customisation of text strings in the command-line version of the game is desired. A lot of useful game state information is printed by default if the returned dict is empty (and even if this function is not defined at all). If you are happy with the default information printed then this function does not need to be modified.

The most common usage of this function is to bring elements of the player's game state into the strings, and for this reason the internal name ``game_state`` (the named tuple with the player's game state) is accessible from the ``cl`` module. The function returns a dict named ``formatted_strings``. The keys in ``formatted_strings`` recognised by the CTGames framework (of which one is unlikely to ever need to change any beyond the first three) are

    'problem_instance'
        A pretty printed version of the problem instance

    'question'
        A formatted version of the question for children

    'rules_text'
        The rules for the current round; an empty string is stored by default if no rules are defined

    'custom_state_info'
        A string representation of the custom game state information (most games will not need to modify this)

    'multiple_choice_answers'
        The multiple choice answers, if an MCQ game, formatted as a single string, each prepended with one of 'A)', 'B)', 'C)', and so on, and each separated separated with '\\n' (most games will not need to modify this)

    'players_answer'
        Feedback for the player: a specially formatted version of the player's answer (most games will not need to modify this)

    'correct_answer'
        Feedback for the player: the correct answer (most games will not need to modify this)

    'helpful_comment'
        Feedback for the player: a helpful comment (most games will not need to modify this)


..
    **HIDDEN**
    The following keys are also part of this dict, but they are overwritten by the framework so cannot be modified by an individual game.
    'level_str'
        Game state information (including the player's level) in a predefined compact representation (this string is created by CTGames as standard so it would be unusual for a game to need to modify it)


If no string is given for a particular key in ``formatted_strings``, a sensible default is given by the CTGames framework.

Several games change the problem instance string, for example, Ordered List

.. code-block:: python

    # Add a string with the problem instance
    problem_instance = f'The list is: {original_list}'
    formatted_strings.update({'problem_instance': problem_instance})

and Just Addition

.. code-block:: python

    # Add a string with the problem instance
    sign = '-' if subtraction else '+'
    problem_instance = f'The sum is: {pair[0]} {sign} {pair[1]} ='
    formatted_strings.update({'problem_instance': problem_instance})


Most games do not need to modify the question as the same question is appropriate for each game round, e.g. "Which path is shortest?" However, some games modify the question, such as Count from Zero

.. code-block:: python

    # Add a formatted string with the question
    question = MAIN_GAME.GAME_QUESTION_CHILDREN.format(PRIZE)
    formatted_strings.update({'question': question})


Molecules changes...

Pollen changes...

Kangaroo changes...








As another example, the game Ordered updates the rules text, problem_instance, and the question in this function.


.. _sect_copyright_statements:

-----------------------------------------
Copyright statements in the template game
-----------------------------------------

(found in "logic.py" and "webapp/__init__.py")

The copyright statements for each game are in files "logic.py" and "webapp/__init__.py". In the template, they have your name by default, such as:

.. code-block:: python

    __copyright__ = 'Copyright 2021, Josephine A. Bloggs'

in the anticipation that you will be making significant changes to the files. All of the changes that you make to the files as part of your studies are owned by you, hence the copyright is assigned to you. You have a choice to transfer/release this copyright, however, as explained below.

These three eventualities are possible if you transfer/release copyright for a particular game:

    - At some point in the near future, the CTGames web apps will be publicly available to a wide audience of teachers and pupils. Your game can be included in that deployment.
    - In the future we may wish to make decisions about the code such as open sourcing parts of CTGames. Your game can be included in that open source git repository.
    - Future developers may wish to improve games in CTGames, either in terms of improved graphics, features, and/or gameplay. Future developers will be able to improve your game.

If you transfer your copyright to Thomas J. Naughton, by changing these lines to

.. code-block:: python

    __copyright__ = 'Copyright 2021, Thomas J. Naughton'

or release your code into the public domain (read about the `CC0 licence <https://creativecommons.org/share-your-work/public-domain/>`_), by changing these lines to

.. code-block:: python

    __copyright__ = 'Released under a CC0 licence'

then your code can be included in the roll-out to schools and teachers all across Ireland and further afield, your game can be included in any open sourced parts of CTGames, and other developers can continue where you left off to improve your game.

.. note:: If you choose to change the copyright statement, you must change it in both files "logic.py" and "webapp/__init__.py".  No-one else can retrospectively change the copyright on your game's code files, so if you don't change both files for a particular game, that game cannot be included in CTGames.

.. note:: If we upload the full git repository that includes your code to a public hosting site such as GitLab.com or GitHub.com, the full history of your commits will be public if you wish to refer to it in your CV, for example, as evidence of your contribution to a large software project.

.. note:: There is no advantage or disadvantage for your final year project, either in terms of the amount of assistance offered by your supervisor or in terms of your final year project grade, whether you choose or not to transfer/release the copyright of your code. By default, it is understood that you own any code you write as part of your undergraduate studies, and by doing nothing with the lines above, you continue to own it.

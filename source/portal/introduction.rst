.. _chap_portal_development:

.. centered:: Chapter: Teacher portal development

==================================
Introduction to the teacher portal
==================================

This chapter introduces the web app and back-end functionality that teachers use. The CTGames games are run in a web browser using `Brython`_. However, the teacher interacts with a portal that is a more conventional web app in that html templates are rendered and displayed to the web browser but all meaningful logic is run on the server side.

The teacher portal

  - authenticates teachers signing in to the portal and students signing in to CTGames
  - allows a teacher to create a class and generate PINs for students
  - allows a teacher to activate a class, thus permitting students from that class to sign in to CTGames using their PINs, and
  - records and returns the progress of students.

The teacher portal interacts with the CTGames games as illustrated in figure ":ref:`fig_interaction_python_other_technologies`", and comprises a Flask server, Nginx web server, PostgreSQL database, each in its own Docker container.

In this chapter, the files that effect the server-side functionality are explained. This chapter is currently in an unpolished form, reflecting a code base that is experimental in nature. Both the server-side code base and this documentation need to undergo structural and fine updates. It is published here in its unpolished form as a temporary measure.

.. _Brython: https://www.brython.info/

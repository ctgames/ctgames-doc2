.. _sect_guidelines_for_web_app_development:

=======================================
Guidelines for web app game development
=======================================

By default, each fully-functional command-line game automatically has as a fully-functional web app version. The framework puts a thin web app layer over the logic code to automatically make this web app. However, it will not be pretty. The main job of developing the web app version is to make the game engaging for children.

.. tip:: The logic file should do all of the heavy calculations when it comes to graphics and animations. If you find that your web app is doing a lot of work, then you need to move this code into the logic file. The reason for this is that we want the framework to be able to support multiple graphical front ends (e.g. desktop, mobile, and alternative web-based game engines). Since the logic file will be common to all front ends, the less code that will be in the web app, the easier it will be for the game to be deployed with a different front end. In particular, the web app should not perform any calculations that are already performed in the logic file (e.g. checking if the player's answer is correct); this opens up the possibility for contradictions to occur (e.g. where the logic will say that an answer is correct but the graphical animation indicates that it is incorrect, or vice versa).


------------------------------------
Simplest web app games to learn from
------------------------------------

One excellent way to learn how to create games with the CTGames framework is to try to understand the code of existing games. Besides the template game, the three simplest games in CTGames are designed to act as learning aids for someone starting out with the framework. They are Just Addition (directory :file:`justaddition`), Just Odd (directory :file:`justodd`), and Just Reverse (directory :file:`justreverse`). Please read and understand their code to get some ideas for how to make a start with your game.

When you have a need to move on to more complicated features/functionality for your game beyond what these simple games provide, try to find existing CTGames games that have that feature or provide that functionality. The full code of each game is available, so you'll quickly be able to see how that game achieved it. Playing a large selection of web app games will also give you ideas for how your game could progress. If there is a feature or piece of functionality that you don't see in any previous game in CTGames, talk to your supervisor to see if and how it could be implemented.


Running locally the web app versions of other developers' games
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. include:: running_web_app_games.rst


--------
Graphics
--------

SVGs are preferred for cartoon-like backgrounds and game elements because they are infinitely scalable and usually smaller in size compared to raster images. JPG images should be used for real-world photos used for backgrounds. PNGs would be acceptable for game title graphics.

Ensure that you have a licence PNG file for each graphic used in your game. This can be obtained by taking a screenshot of the webpage where it was downloaded from (with URL visible). This screenshot, saved in PNG format, should be in the :file:`licence/` directory for your web app with a name similar to that of the original graphic in your game.

Filenames should all be in lowercase to conform to a Python convention to support cross-platform compatibility.

.. admonition:: Image resources

    Download SVGs with favourable licence terms from (for example):

    .. hlist::
        :columns: 2

        - https://pixabay.com/
        - https://openclipart.org/
        - https://publicdomainvectors.org/
        - https://www.freepik.com/

    Download photos for game background images from (for example):

    .. hlist::
        :columns: 2

        - https://pixabay.com/
        - https://unsplash.com/
        - https://www.freepik.com/

    SVG files can be made smaller (in terms of bytes) using:

        - https://github.com/RazrFalcon/svgcleaner
        - https://www.sarasoueidan.com/blog/svgo-tools/


-----------------
Web app debugging
-----------------

Some tips to get you started debugging your web app programs:

- To see error messages from a web app, right-click on a background area of the web app and choose the option that contains the word :guilabel:`Inspect` (Firefox and Chromium/Chrome use this term), then select the tab labelled :guilabel:`Console`.

- With Brython, :py:func:`print` calls in your Python code will also print to this console.

- To inspect the properties of a DOM element, left-click the arrow that is a couple of symbols to the left of, and on the same row as, the :guilabel:`Console` tab, then left-click the DOM element you wish to inspect, and the properties of that DOM element will instantly appear in a tab labelled :guilabel:`Inspector` (using Firefox) or labelled :guilabel:`Elements` (using Chromium/Chrome).

- After changing your web app code, the quickest way to see the updated behaviour in the web app is to first click the hamburger menu (the :guilabel:`☰` icon), and then click :guilabel:`Reload game from server` (this just reloads the code for your web app, and leaves the CTGames framework in memory). If the hamburger menu is not visible (if CTGames crashed, for example), then you will have to type :kbd:`Ctrl-r` on the keyboard to reload the CTGames framework and sign in again.


------------------------------------
Conveniences for web app development
------------------------------------

A local preferences file can be found in your local copy of the git repo at :file:`CTGames/ctgames/` (or, equivalently, at :file:`CTGames/bin/server/docker/app/static/ctgames/`) with the name :file:`local_player_preferences.py`. It is explained below how you should edit it. It is intentional that this file is ignored by git, so you should push your edits to the git repository.


Player name and PIN
^^^^^^^^^^^^^^^^^^^

    When signing in to the CTGames web app, you will notice that for privacy reasons (schoolchildren will usually share computers at school) the browser will not store player sign-in details. As a convenience for developers, this can be bypassed, by using the file :file:`local_player_preferences.py` to store a schoolchild's name and PIN as shown::

        saved_player_name = 'ExampleA1'
        saved_player_pin = '0000'


Restricted game menu
^^^^^^^^^^^^^^^^^^^^

    The game menu can take a while to load when there are many games for which to download previous scores, progress, and so on. If you want to just display one or a limited number of game categories, a setting in the file :file:`local_player_preferences.py` can be used to list the names of those categories as follows::

        visible_categories = ['Students\' games under construction', 'Science Week 2018', 'Strategies: backtracking']

    The names of the categories and the list of games in each category are defined in :file:`CTGames/ctgames/gamescategories.py`. If :py:data:`visible_categories` is set to the empty list (the default), as follows::

        visible_categories = []

    then all game categories will be displayed.

================================
The file :file:`webapp/names.py`
================================

This file is used for constants, metadata, and initialisation calculations that can be common to many games. These names are separated to help ensure consistency in the order they are defined and to make :file:`__inti__.py` a little less cluttered.

---------------------
Creating the tutorial
---------------------

The tutorial that you create for your game should be aimed at the child player. It should not be too cluttered and should not confuse a child by having pictures of buttons that a child might be fooled into clicking. Each tutorial should have two examples, a very easy one and a harder one.

TODO

Open a terminal in any directory, and enter
::

    cd ~/gitlab.cs.nuim.ie/ctgames/ctgames/CTGames/bin/
    git pull

To start the docker containers with web server, Flask server, and database server, within the CTGames Conda environment you can type (without pressing :kbd:`Enter`)
::

    ./sta

Press :kbd:`Tab` and it should autocomplete to :command:`./start_docker_server`. Then press :kbd:`Enter`.

    .. note:: You do not need to be in the CTGames Conda environment when you run :command:`./start_docker_server`. If you do, you will see an error of the form ``activate: No such file or directory``; you can safely ignore this error. Also, ignore an error of the form ``postgresql: command not found``; it just represents a check in case you are running a local version of PostgreSQL.

You will have to leave this program running in the terminal as long as you need the web server to be active. So, if you have other work to do on the command line, you'll have to open a new terminal.

    .. tip:: If you are using the VirtualBox image for CTGames development, a teacher name, email, and password have been created for you and are in a text file on the Ubuntu desktop called :file:`~/Desktop/Player PINs.txt`. This file also contains a class name and class members' IDs and PINs.

        Alternatively, if you are installing CTGames directly on your computer according to the previous instructions, you will have already created the class members' IDs and PINs by this point.


The teacher credentials are needed to activate the class (to allow class members to sign in). Point Firefox to `<http://127.0.0.1:8080/teachers>`_ and sign in as a schoolteacher. Choose the appropriate class and click :guilabel:`Activate`.

Any of the player credentials can be used to play the web-based versions of games. Point Firefox to `<http://127.0.0.1:8080/>`_ and sign-in as one of the class members using the appropriate ID and PIN.

To stop the server, go back to the terminal. Enter :kbd:`Ctrl-c` and wait a couple of seconds for the server to shut down.
